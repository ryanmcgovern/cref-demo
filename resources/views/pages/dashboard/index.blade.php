<main>
	<p>Hello, Name Surname - welcome to your dashboard</p>
	@include('pages.dashboard.manage')
	@include('pages.dashboard.stats')
	@include('pages.dashboard.assignments')
	@include('modals.share-link')
</main>
