<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display the home page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'home';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the login page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'login';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the register page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'register';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the forgotten password page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function forgotten_password(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'forgotten-password';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the dashboard page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'dashboard';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the add asignment page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_assignment(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'dashboard.add-assignment';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the single assignment page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function single_assignment(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'dashboard.single-assignment';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the reference request page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reference_request(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'dashboard.reference-request';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the contact page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'dashboard.contact';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the shared assignments page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function shared_assignments(Request $request)
    {

        $page = new \App\Page;
        $page->template = 'shared-assignments';
        return view('page', [
            'page' => $page
        ]);
    }

    /**
     * Display the default page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function default(Request $request)
    {

        $page = new \App\Page;
        return view('page', [
            'page' => $page
        ]);
    }

}
