<main>
	<p>Single assignment</p>
	@include('pages.dashboard.single-assignment.details')
	@include('pages.dashboard.single-assignment.cta')
	@include('pages.dashboard.single-assignment.requested')
	@include('pages.dashboard.single-assignment.complete')
	@include('pages.dashboard.single-assignment.uploaded')
	@include('pages.dashboard.single-assignment.cta')
	@include('modals.request-reference')
	@include('modals.upload-reference')
</main>
