<main>
	<p>Reference request</p>
	@include('pages.dashboard.reference-request.details')
	@include('pages.dashboard.reference-request.form')
	@include('pages.dashboard.reference-request.cta')
</main>
