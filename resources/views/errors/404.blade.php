@extends('default-layout')

@section('content')
	@include('components.header')
	<p>Error page</p>
	@include('components.contact')
	@include('components.footer')
@endsection
