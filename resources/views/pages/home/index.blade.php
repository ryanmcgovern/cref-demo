<main>
	@include('pages.home.hero')
	@include('pages.home.how-it-works')
	@include('pages.home.reasons-to-register')
	@include('pages.home.about-us')
	@include('pages.home.testimonials')
	@include('components.contact')
</main>
