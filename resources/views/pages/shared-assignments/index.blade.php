<main>
	<p>References</p>
	@include('pages.shared-assignments.details')
	@include('pages.shared-assignments.assignments')
	@include('pages.shared-assignments.cta')
	@include('components.contact')
</main>
