<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');
Route::get('/login', 'PageController@login')->name('login');
Route::get('/register', 'PageController@register')->name('register');
Route::get('/forgotten-password', 'PageController@forgotten_password')->name('forgotten-password');
Route::get('/dashboard', 'PageController@dashboard')->name('dashboard');
Route::get('/dashboard/add-assignment', 'PageController@add_assignment')->name('add-assignment');
Route::get('/dashboard/assignment', 'PageController@single_assignment')->name('single-assignment');
Route::get('/dashboard/reference-request', 'PageController@reference_request')->name('reference-request');
Route::get('/dashboard/contact', 'PageController@contact')->name('contact');
Route::get('/shared-assignments', 'PageController@shared_assignments')->name('shared-assignments');

Route::get('/default', 'PageController@default'); // default page if no specific template needed
