@extends('default-layout')

@section('content')
	@include('components.header')
	@if(empty($page) || empty($page->template))
		@include('pages.default.index')
	@else
		@include('pages.'.$page->template.'.index')
	@endif
	@include('components.footer')
@endsection
